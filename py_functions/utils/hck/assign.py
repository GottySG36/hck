
import sys
import re

# read ITS_metagenome.2D.kraken

def assign(infile_1, infile_2, outfile):
    dico = {}

    with open(infile_1, "r") as in_1:
        for line in in_1:
            dico[ ( line.strip().split("\t") )[1] ] = line.strip()


    # read cluster file
    with open(infile_2, 'r') as in_2:
        with open(outfile, 'w') as out:
            re_clust = re.compile("^Cluster")

            line = in_2.readline()

            # first line = cluster name
            clust_name = line.strip()
            clust_members = []  # contains all members of cluster

            line = in_2.readline()

            while line:
                clust_id = line.strip()
                if re_clust.match(clust_id):  # new cluster
                    # print all cluster + metadata in dico
                    # to_print = C_name + "\n"
                    to_print = ""
                    # get dico right key
                    right_key = ""
                    for member in clust_members:
                        if dico.get(member, None) != None:
                            val = dico[ member ].strip().split("\t")
                            val_0 = val[ 0 ]+'\t'  # code
                            val_1 = "\t".join(val[2:])

                        else:
                            val_0 = ""
                            val_1 = ""

                        write = val_0 + member +"\t"+ val_1 + '\n'
                        out.write(write)

                    # new_cluster
                    clust_name = clust_id
                    clust_members = []
                    line = in_2.readline()

                else:
                    clust_members.append(line.strip())
                    line = in_2.readline()

            # last cluster
            # to_print = C_name + "\n"
            to_print = ""
            right_key = ""
            for member in clust_members:
                if dico.get(member, None) != None:
                    val = dico[ member ].strip().split("\t")
                    val_0 = val[ 0 ]+'\t'  # code
                    val_1 = "\t".join(val[2:])

                else:
                    val_0 = ""
                    val_1 = ""

                write = val_0 + member +"\t"+ val_1 + '\n'
                out.write(write)
