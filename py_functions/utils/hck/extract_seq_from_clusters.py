
def extract_seqs(infile_1, infile_2, outfile):
    """
    usage : extract_seqs(<input_file_1>, <input_file_2>, <output_file>)

    Takes a database file and the extracted representative members from a series of clusters and gets the corresponding sequences from the database

    keyword arguments :
    - infile_1  : STR, Filename (PATH) for the database to use
    - infile_2  : STR, Filename (PATH) for the clusters to use
    - outfile   : STR, Filename (PATH) in which to write the resulting sequences

    return :
    - None      : Directly writes to <output_file>
    """
    dico = {}

    with open(str(infile_1), 'r') as in_1:

        header  = in_1.readline()
        seq     = in_1.readline()

        while header:
            dico[ header.strip().split()[0] ] = seq.strip()
            header  = in_1.readline()
            seq     = in_1.readline()

#    [print("{} : {}".format(key, dico[key])) for key in list(dico.keys())[:100]]

    with open(str(infile_2), 'r') as in_2:
        with open(str(outfile), 'w') as out:
            line = in_2.readline()
            while line :
                if dico.get(line.strip(), None) != None:
                    write = "{}\n{}\n".format(line.strip(), dico[ line.strip() ].strip())
                    out.write(write)
                else:
                    print("ERROR -:- Could not find {}".format(line.strip().split()[0]))
                line = in_2.readline()
