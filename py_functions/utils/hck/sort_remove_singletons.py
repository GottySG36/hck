
import os, sys

def remove_singletons(infile, outfile):
    """
    usage : remove_singletons(<input_file>, <output_file>)

    Take a cluster list and remove clusters containing only one member.

    keyword arguments :
    - infile    : STR, the filename of the list of clusters
    - outfile   : STR, the filename for the list of cleaned clusters

    return :
    - None      : Directly write to <outfile>
    """
    with open(str(infile),'r') as data_in:
        with open(str(outfile), 'w') as data_out:
            nb = 0
            for line in data_in:
                if line [0] == '>':
                    nb = 0
                    header = line
                if line[0] != '>':
                    nb += 1
                    if nb == 1:
                        line1 = line
                    elif nb == 2:
                        write = "{}{}{}".format(header, line1, line)
                        data_out.write(write)
                    elif nb > 2:
                        data_out.write(line)
    data_in.close()
    data_out.close()

def extract_rep_seq(infile, outfile):
    """
    usage : extract_rep_seq(<input_file>, <output_file>)

    Take a cluster list containing no singletons and extracts the representative sequence for each cluster.

    keyword arguments :
    - infile    : STR, the filename of the list of cleaned clusters to sort
    - outfile   : STR, the filename for the list of representative members for each clusters

    return :
    - None      : Directly write to <outfile>
    """
    with open(str(infile),'r') as data_in:
        with open(str(outfile), 'w') as data_out:
            nb = 0
            for line in data_in:
                if line[-2] == '*':
                    txt = line.split(' ')[1][:-3] + '\n'
                    data_out.write(txt)

    data_in.close()
    data_out.close()
