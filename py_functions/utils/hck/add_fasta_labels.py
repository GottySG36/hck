import sys
from os.path import basename, splitext
from pathlib import Path
import re

def add_labels(fasta_in, fasta_out):
    """
    Usage : add_labels([LIST_IN], <FILE_OUTt>)

    Takes fasta files and fuses them into one. Also adds the sample name and 
    adds a serial number in the header of each fasta sequence.

    Also renames the sequences by replacing any underscores ('_') by dots ('.').
    Writes the new and old names to file "fasta/equivalence.csv" in the format :
    - <file_name_original>,<file_name_renamed> 

    KEYWORD arguments:
    fasta_in    -- A list containing all the paths to to the fasta files to merge
    fasta_out   -- The path of the output fasta file

    RETURN:
    None        -- Directly writes to <FILE_OUT>
    """
    listing_unlabeled = fasta_in
    labeled = fasta_out
    lab = open(labeled,'w')
    equiv = open("fasta/equivalence.csv", 'w')

    # counter = 0 # Set counter here to have continuous numbering across samples
    for item in listing_unlabeled:
        counter = 0 # Set counter here to have numbering reset to zero every samples
        base = basename(splitext(item)[0])

        ## Renames by replacing '_' by '.'
        base_renamed = re.sub('_', '.', base)

        ## Writes to 'equivalence.csv' file
        equiv_line = "{0},{1}\n".format(base, base_renamed)
        equiv.write(equiv_line)
        
        with open(item, 'r') as unl:
            unlabeled_header = 1
            seq = 1
            new = ''
            
            while unlabeled_header:
                lab.write(new)
                counter += 1
                unlabeled_header = unl.readline()[1:]
                seq = unl.readline()
                labeled_header = ">{0}_{1} {2}".format(base_renamed, counter, unlabeled_header)
                new = labeled_header + seq
        
        unl.close()
    
    lab.close()
    equiv.close()
