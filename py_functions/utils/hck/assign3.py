
import re

def _get_item(id, d_rmeta):
    if d_rmeta.get(id, None) != None:
        return d_rmeta[id].split(id)

def assign(rep_meta, sorted_list, output):
    """
    usage : assign(<rep_metagenome>, <sorted_cluster_list>, <output_file>)

    Reads the kraken output (rep_metagenome) and extracts the metadata for each representative sequence.
    It then pastess that information to every member of the cluster containing the rep seq. 

    keyword argument :
    - rep_meta    : (STR) File containing the kraken output 'rep_metagenome.kraken'
    - sorted_list : (STR) File containing the different cluster and their members, sorted alphabetically.
    - output      : (STR) File in which the completed metagenome will be written to.

    return :
    - None : Directly writes to .output' file
    """
    r_meta = {} # dict, will contain our rep_metagenome
    with open(rep_meta, "r") as r:
        for line in r:
            r_meta[ ( line.strip().split("\t") )[1] ] = line.strip()
    
    # Reading cluster and storing the names as STR, will split and parse after
    with open(sorted_list, 'r') as sorted_list:
        clusters_all = sorted_list.read()
    
    # Seperating every cluster, then splitting the names to obtain 
    # a LIST of clusters LISTing the cluster members
    regex = re.compile(r'Cluster\d+')
    clusters = re.split(regex, clusters_all)
    clusters = [ clust.strip().split('\n') for clust in clusters][1:]

    # Filtering to keep only the information of the rep seq of every cluster
    tags = []
    for i in range(len(clusters)):
        tags += [ _get_item(id, r_meta) for id in clusters[i] ]
    tags = [ item for item in tags if item != None ]
    
    # Pasting the information
    final = []
    for i in range(len(clusters)):
        final += [ "{}{}{}\n".format(tags[i][0], id, tags[i][1]) for id in clusters[i] ]

    with open(output, 'w') as out: out.write("".join([ "".join(clust) for clust in final]))

