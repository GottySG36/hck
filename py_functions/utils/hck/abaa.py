

import time as t
import itertools as it
import numpy as np
import sys
import os
import re
import argparse

__version__ = 0.2
__authoris__ = "Antoine Bodein, Simon Gotty"

# regex
fasta_reg = re.compile(r"(?i)[\.|_]f(a|asta|na)$")


def read_params():
    parser = argparse.ArgumentParser(epilog=
            "This utility script is split in four (4) steps : \n"
            "\tSTEP 1 : This step is responsible for parsing the fasta files and extracting the sequences' lengths and occurences. \n"
            "\tSTEP 2 : This step calculates the statistics and cutoff value.\n"
            "\tSTEP 3 : This step filters the sequences' lengths using the cutoff found in step2.\n"
            "\tSTEP 4 : This final step filters the fasta file.", formatter_class=argparse.RawTextHelpFormatter)


    parser.add_argument("-i", "--input", metavar="<input_dir>", required=True, type=str, help="Input directory with joined fasta files")
    parser.add_argument("-o","--output", metavar="<output_dir>", required=True, type=str, help="Output directory to write filterd fasta files")
    parser.add_argument('-f', '--force', action="store_true", default=False, help="Force writing in directory if already exists")
    parser.add_argument('--version', action='version', version='%(prog)s {version}'.format(version=__version__))


    return parser.parse_args()


def check_input(input_filepath):
    """
    check if filepath is valid,
    and the directory contains any fasta files
    """
    if not os.path.exists(input_filepath):
        exit("Error: <input dir> is not a valid filepath")

    if not os.listdir(input_filepath):
        exit("Error: <input dir> is empty")

    if not [f for f in os.listdir(input_filepath) if fasta_reg.search(f)]:
    # identify .fa or .fasta file
        exit("Error: <input dir> contains any .fa, .fasta or .fna files.")


def check_output(output_filepath, force):
    """
    check :
    - if output and not force : raise
    - else : create dirs, raise OSError if permission denied
    """

    if not os.path.exists(output_filepath):
        try:
            os.makedirs(output_filepath)  # possible permission denied
        except:
            exit("Permssion denied! You can't write in <output dir>.")

    else:  # outpath not already exists
        if not force:
            exit("WARNING: Output folder already exists, please use -f/--force to overide it.")


def filter_fna_len(file_in):
    """
    usage : filter_fna_len(<file_in>, <chunk_size>)

    Reads <file_in> and for every sequence extracts the sequence length and
    how many times that length is found (number of occurence)

    keyword argument :
    - file_in    : FILE containing the sequences to filter
    - chunk_size : INT specifying how many lines should be loaded into memory at a time

    return :
    - lengths    : LIST containing the length of every sequences in <file_in>
    - dict_occur : DICTionnary whose KEYS are sequences' lengths and VALUES are
                   the number of occurences for that specific length
    """
    lengths = []
    dict_occur = {}

    with open(file_in, 'r') as content:

        header = ''
        seq = 'FIRST'
        count = 0

        ## Go through current chunk and for every sequence do the following
        ##   1. Calculate the sequence length and append to LENGTHS list
        ##   2. Calculate the sequence length and add its occurence to DICT_OCCUR
        for line in content:
            if line[0] == '>':

                if seq != 'FIRST':
                    ## add occurence to DICT_OCCUR
                    length = len(seq)
                    try:
                        dict_occur[length] += 1
                    except KeyError:
                        dict_occur[length] = 1

                    lengths.append(length)

                seq = ''
                header = line.strip()

            else:
                seq += line.strip()

        lengths.append(length)
        try:
            dict_occur[length] += 1
        except KeyError:
            dict_occur[length] = 1

    return lengths, dict_occur


def get_stats(length_list):
    """
    usage : get_stats(<length_list>)

    Got through the <length_list> integer list and calculate some statistics
    - Min, Max, Mean, Standard Deviation, Median

    keyword arguments:
    - length_list :

    return :
    - dict : {MIN, MAX, AVG, STD, MED}
    """
    len_list = np.array(length_list)
    length_min = np.min(len_list)
    length_max = np.max(len_list)
    length_avg = np.mean(len_list)
    length_std = np.std(len_list)
    length_med = np.median(len_list)

    return {"MIN":length_min, "MAX":length_max, "AVG":length_avg, "STD":length_std, "MED":length_med}


def write_stats(length_list, filepath_out):
    """
    usage : write_stats(<length_list>, <filepath_out>)

    Takes a list of integers, calculates some basic statistics  and writes them to file

    keyword arguments:
    - length_list  : LIST of integers to use
    - filepath_out : STRing specifying where to write the statistics

    return :
    - NONE
    """
    stats = get_stats(length_list)
    with open(filepath_out, 'w') as outfile:
        line = "{:<6s} : {:4.2f} \n{:<6s} : {:4.2f} \n{:<6s} : {:4.2f} \n{:<6s} : {:4.2f} \n{:<6s} : {:4.2f}".format(
          "MIN",stats["MIN"], "MAX",stats["MAX"], "AVG",stats["AVG"], "ST DEV",stats["STD"], "MEDIAN",stats["MED"])
        outfile.write(line)


def get_cutoff(dict_occurence):
    """
    usage : get_cutoff(<dict_occur>)

    keyword arguments:
    - dict_occurence :  DICTionnary whose KEYS are sequence lengths and VALUES are
                        the number of occurences for that specific length

    return :
    - cutoff :          Value determining if a sequence is to be discarded or kept
    """

    list_occur = np.array(list(dict_occurence.values()))
    occur_avg = np.mean(list_occur)
    occur_std = np.std(list_occur)

    cutoff = occur_avg + occur_std

    return cutoff


def get_good_length(length_list, dict_occur, cutoff_value):
    """
    usage : get_goog_legth(<length_list>, <cutoff_value>)

    From a list of sequences' lengths, keep or discard sequence if the number of occurences
    for the specific length is GREATER THAN OR EQUAL TO cutoff_value

    keyword arguments:
    - length_list :      LIST of integers to filter
    - dict_occur :       DICTionnary whose KEYS are sequence lengths and VALUES are
                         the number of occurences for that specific length
    - cutoff :           INT value determining if a sequence is to be discarded or kept

    return :
    - dict_is_good_len : DICT whose KEYS are sequences' lengths and VALUES are booleans.
                         TRUE if length is to keep, FALSE to discard.
    """
    ## Filter sequence lengths
    dict_is_good_len = {}

    list_good_lengths = list(filter(lambda length: dict_occur[length] >= cutoff_value, dict_occur.keys()))

    ## Initialize DICT
    for nb in dict_occur.keys():
        dict_is_good_len[nb] = 0

    ## Set values to TRUE if values are >= cutoff
    for good_nb in list_good_lengths:
        dict_is_good_len[good_nb] = 1

    return dict_is_good_len


def write_fasta_length(filepath_in, filepath_out, dict_to_keep):
    """
    usage : write_fasta_length(<filepath_in>, <filepath_out>, <dict_to_keep>)

    keyword arguments :
    - filepath_in  : FILE name containing the sequences to filter
    - filepath_out : FILE name in which to write the filtered sequences
    - dict_to_keep : DICT whose KEYS are sequences' lengths and VALUES are booleans.
                     TRUE if length is to keep, FALSE to discard.

    return :
    - NONE :         Directly writes to <filepath_out>
    """
    with open(filepath_out, 'w') as filtered:
        with open(filepath_in, 'r') as content:
            header = ''
            seq = 'FIRST'
            for line in content:
                if line[0] == '>':
                    if seq != 'FIRST':
                        if dict_to_keep[len(seq)]:
                            newline = "{}\n{}\n".format(header, seq)
                            filtered.write(newline)

                    seq = ''
                    header = line.strip()

                else:
                    seq += line.strip()

            if dict_to_keep.get(len(seq), None) != None:
                newline = "{}\n{}\n".format(header, seq)
                filtered.write(newline)



if __name__ == '__main__':
    # global_start = t.time()

    args = read_params()

    chunk_size = args.chunksize

    print("{:<12s} : {}".format("Chunk size",chunk_size))

    check_input(input_filepath=args.input)

    check_output(args.output, args.force)

    PATH_IN = args.input
    PATH_OUT = args.output

    files_in = list(filter(lambda name: fasta_reg.search(name), os.listdir(args.input)))

    for to_filter in files_in:
        # file_start = t.time()

        filename, ext = os.path.splitext(to_filter)
        input_file = "{}/{}".format(PATH_IN, to_filter)
        output_file = "{}/{}_filtered{}".format(PATH_OUT, filename, ext)
        filepath_stat = "{}/{}.stat".format(PATH_OUT, filename)

        print("Now filtering {}...".format(input_file))

        ## Step 1 : Extract lengths and count occurences
        lengths_all, occurences_dict = filter_fna_len(input_file, args.chunksize)

        # step1 = t.time()
        # step = step1 - file_start
        # print("\t- Step 1 : {:4}m {:2}s".format(int(step//60), round(step%60,2)))

        ## Step 2 : Get the statistics
        write_stats(lengths_all, filepath_stat)

        # step2 = t.time()
        # step = step2 - step1
        # print("\t- Step 2 : {:4}m {:2}s".format(int(step//60), round(step%60,2)))

        ## Step 3 : Filter sequences
        cutoff_val = get_cutoff(occurences_dict)
        dict_is_good = get_good_length(lengths_all, occurences_dict, cutoff_val)

        # step3 = t.time()
        # step = step3 - step2
        # print("\t- Step 3 : {:4}m {:2}s".format(int(step//60), round(step%60,2)))


        ## Step 4 : Write the filtered files
        write_fasta_length(input_file, output_file, dict_is_good)

        # step4 = t.time()
        # step = step4 - step3
        # print("\t- Step 4 : {:4}m {:2}s".format(int(step//60), round(step%60,2)))

        # step = t.time() - file_start
        # print("\t- File {} took : {:4}m {:2}s".format(input_file, int(step//60), round(step%60,2)))
        # print("#"*80)


    # step = (t.time() - global_start)
    # print("Total execution time : {:4}m {:2}s".format(int(step//60), round(step%60,2)))
