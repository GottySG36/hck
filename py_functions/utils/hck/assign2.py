#!/usr/bin/python2
import sys
import re

# read ITS_metagenome.2D.kraken
IN = sys.argv[1]
dico = {}

f = open(IN, "r")
line = f.readline()

while line:
    tmp = line.strip().split("\t")
    dico[tmp[1].split(";")[0]] = line.strip()
    line = f.readline()

f.close()

# read cluster file
g = open(sys.argv[2])
Clust = re.compile("^Cluster")


line = g.readline()
# first line = cluster name
C_name = line.strip()
C = []  # contains all members of cluster

line = g.readline()

while line:
    ID = line.strip()
    if Clust.match(ID):  # new cluster
        # print all cluster + metadata in dico
        # to_print = C_name + "\n"
	to_print = ""
        # get dico right key
        right_key = ""
        for i in C:
            if i in dico.keys():
                right_key = i
        try:
            val = dico[right_key].strip().split("\t")
            val_0 = val[0]+'\t'  # code
            val_1 = "\t".join(val[2:])
            for i in C:
            	to_print = val_0 + i +"\t"+ val_1
		print to_print.strip()
        except:
            val_0 = ""
            val_1 = ""
#        for i in C:
#           to_print += val_0 + i +"\t"+ val_1 + '\n'
        # print to_print,

        # new_cluster
        C_name = ID
        C = []
        line = g.readline()

    else:
        C.append(line.strip())
        line = g.readline()
g.close()

# last cluster
# to_print = C_name + "\n"
to_print = ""
right_key = ""
for i in C:
    if i in dico.keys():
        right_key = i
try:
    val = dico[right_key].strip().split("\t")
    val_0 = val[0]  # code
    val_1 = "\t".join(val[2:])
    for i in C:
#    	to_print += val_0 + i +"\t"+ val_1 +'\n'
        to_print = val_0 + i +"\t"+ val_1
	print to_print.strip()
except:
    val_0 = ""
    val_1 = ""
#for i in C:
#    to_print += val_0 + i +"\t"+ val_1 +'\n'
#print to_print,
