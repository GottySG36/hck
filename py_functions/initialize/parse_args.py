import argparse
from pathlib import Path
from py_functions.utils.colors import *

cwd = Path.cwd()
DEFAULT_INDEX = "sm_utils/config/RNASEQ/index.idx"
#DEFAULT_INDEX = "/is1/commonDatasets/kallisto_idx/Homo_sapiens_ensembl_release-95/Homo_sapiens.GRCh38.cdna.standardchr_95.fa.gz.idx"


def get_args():
    parser = argparse.ArgumentParser(epilog=
    "This utility generates the different config files used in different types of analysis.\n"
    "The currently supported study types are the following:\n"
    " - 16S\n"
    " - ITS\n"
    "\n"
    "**  See README for a more detailed explanation of all the required files and tools.\n"
    "\n"
    "IMPORTANT NOTE\n"
    "This script should always be called from the root of the Snakemake_Workflows pipeline.\n"
    "It was intended to be able to find all the required files from there and won't work otherwise.\n"
    "\n"
    "Also note that this script is written in python3 and therefore will only work if called\n"
    "using python3, in case of bugs, make sure you are using the proper python version prior\n"
    "to doing anything else.\n"
    "\n"
    "Report bugs on package home repository\n"
    "pkg home page: <https://bitbucket.org/GottySG36/hck/src/master>",
    formatter_class=argparse.RawTextHelpFormatter, add_help=False)

    ## MANDATORY arguments
    required = parser.add_argument_group('required arguments')

    required.add_argument("-i", "--info-sheet", required=True, type=str, 
            help="Specify the info sheet to use.")
    

##################################################    
# Common arguments

    common = parser.add_argument_group('common arguments')

    required.add_argument("-s", "--study-type", default = '16S', type=str.upper, required=False, 
            choices = ['16S', 'ITS'], help="Specify the type of data used.")

    common.add_argument("-f","--force", action="store_true", help="This parameter will cause the script "
        "to overwrite the Snakefile and any data \npresent in the 'fastq' directory instead of throwing an error.")

    common.add_argument("-h", "--help", help="Show this help message and exit", action="help")

##################################################
# HCK arguments
    HCK = parser.add_argument_group('HCK arguments', description = 'Arguments that are specific to HCK steps')
    HCK.add_argument("-kdb", "--kraken-db", type = str, default = "config/kraken_db", help = "Specify the PATH to the kraken database to use.\n** Note that this pipeline does not provide any database. We cannot know what\ndata you want to work with so it is your responsability to find the appropriate\ndatabase for your dataset.")


##################################################
# Specific requirements definition

    return parser.parse_args()

