import pandas as pd

def sort_info_sheet(info_sheet):

    info_sheet.sort_values(['sample_name', 'pair', 'lane'], axis = 0).to_csv(".tmp_data_snakemake/info_sort.csv", index = False)
    return info_sheet.sort_values(['sample_name', 'pair', 'lane'], axis = 0)
