
import yaml
from pathlib import Path

def load_conf(in_file):
    cwd = Path.cwd()
    with cwd.joinpath(in_file).open(mode = 'r') as conf:
        conf_load = yaml.safe_load(conf)
    
    return conf_load


def write_conf(data, out_file, mode):
    cwd = Path.cwd()
    with cwd.joinpath(out_file).open(mode = mode) as conf:
        yaml.dump(data, conf, default_flow_style=False, allow_unicode=True, indent = 4)
