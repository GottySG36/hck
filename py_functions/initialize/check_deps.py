
import sys

def check_deps():
    missing_deps = []

    if sys.version_info[0] != 3 or sys.version_info[1] < 5:
        sys.exit("ERROR -:- This script requires Python version 3.5 or above")

    try:
        import pandas as pd
    except ImportError as IError:
        missing_deps.append('pandas')

    try:
        import yaml
    except ImportError as IError:
        missing_deps.append('yaml')

    if len(missing_deps) > 0:
        print('Some modules required by this utility are missing : \n{}'.format(
              '\n'.join(['\t- ' + item for item in missing_deps])))

        sys.exit("ERROR -:- See above for missing modules.\n"
                 "Use the proper tools to install these dependencies (condda, pip, ...),\n" 
                 "then run the script again. See 'README.md' for more information.")


