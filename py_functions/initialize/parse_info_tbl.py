
from py_functions.initialize.exceptions import *
from py_functions.utils.colors import *
import pandas as pd
from pathlib import Path
import os


def parse_info_tbl(file_info_sheet):
    info = pd.read_csv(file_info_sheet)
    info.reindex(["raw_files", "sample_name", "pair", "lane"], axis = 1)

    try:
        not_found = []
        dict_raw = {}
        dict_info = {}

        # Check in info-sheet, if the raw_files cannot be found they are added 
        # in a list (not_found)
        for f in info.raw_files:
            if not Path(f).is_file():
                not_found.append("{:<5s} : {}".format("-->", f))


        ## Throws error MissingFilesInfoSheet if not_found not empty
        if len(not_found) > 0:
            not_found.sort()
            raise MissingFilesInfoSheet


        return info

    except MissingFilesInfoSheet as err:

        print("The following files listed in '{}{}{}' could not be found\n"
              "The following files could not be found in either the info-sheet or the raw-dir:".format(YELLOW, file_info_sheet, DEFAULT))

        for i in range(0, len(not_found)):
            print("{}{} : {}{}".format(RED, i+1, not_found[i], DEFAULT))

        return info

    except FileNotFoundError as err:
        print("{}{}{} could not be found. Please verify than the correct path was given or correct any spelling mistakes.\n"
              "If used directly in a Snakefile, see config_params.txt and look for 'raw_data_directory'. This line / section should contain the directory name.".format(RED, err, DEFAULT))
        exit()

    except AttributeError as err:
        print("\n"
              "File ({4}) header improperly formatted. Column {0}{1}{2} doesn't match any of the required column names.\n\n"
              "Header should have up to four (4) columns. These columns have to be named has follow :\n"
              "-- {3}raw_files{2} : complete name of the file containing the raw data\n"
              "-- {3}sample_name{2} : the name you wish to give to the raw sample\n"
              "-- {3}pair{2} : contains either 'R1' and 'R2'. This column has to be omitted if th data isn't paired\n"
              "-- {3}lane{2} : contains the lane number for each sample. This column has to be omitted if the data isn't split in lanes\n".format(RED, err, DEFAULT, YELLOW, file_info_sheet))
        exit()
