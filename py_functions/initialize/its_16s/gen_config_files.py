
from py_functions.initialize.exceptions import *
from py_functions.utils.colors import *
from pathlib import Path
import sys

def config_yaml(conf, list_study_types, list_lanes, date, extras):
    cwd = Path.cwd()
    study_types = ["COMMONS"] + list_study_types

    conf_file = conf

    for c_type in study_types:
        config_dir = '16S_ITS' if c_type in ['16S', 'ITS'] else c_type
        conf_file['envs'][config_dir] = "../../config/{0}/env.yml".format(config_dir)

    conf_file['mode'] = 'paired' if 'paired' in extras else 'single'
    conf_file['timestamp'] = "{}".format(date)
    conf_file['study_type'] = "{}".format(study_types[-1])

    conf_file['biom_md5'] = "temp"
    conf_file["multiqc_dir"] = ["merge","trimmed"]
    for item in list_lanes:
        conf_file["multiqc_dir"].append("raw_data/"+item)

    return conf_file
