
from pathlib import Path
from py_functions.initialize import parse_yaml as pyml

def gen_config_hck(conf, hck_conf, ini_args, extras):
    c_hck = pyml.load_conf(hck_conf)
    if ini_args.kraken_db != "config/kraken_db":
        c_hck['kraken']['db'] = ini_args.kraken_db

    conf["envs"]['HCK'] = '../../../config/env.yaml'

    return {**conf, **c_hck}

