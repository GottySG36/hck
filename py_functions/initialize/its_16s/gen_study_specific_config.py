
from pathlib import Path       
from py_functions.initialize import parse_yaml as pyml

def study_specific_config(conf, dict_args):
    """
    usage: study_specific_config(<conf>, <dict_of_args>)

    Takes a config file and a dictionary whose KEYS are the config KEYWORDS and the values are the
    specific value for that same KEYWORD

    keyword arguments:
    - conf      : config file (yaml formatted) to update
    - dict_ARGS : DICTionary containing the specific configuration options.
                  KEYS   : parameter name used by snakemake
                  values : parameter value to be use for the specified KEYWORD
    return
    - conf_file : Updated config file
    """

    conf_file = conf

    for arg in dict_args.keys():
        
        conf_file[arg] = dict_args[arg]

    c_file_16s_its = Path.cwd().joinpath("sm_utils/config/16S_ITS/config_params.txt")
    c_16s_its = pyml.load_conf(c_file_16s_its)

    return {**conf, **c_16s_its}
