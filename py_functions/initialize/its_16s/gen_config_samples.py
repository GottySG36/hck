
from pathlib import Path
from py_functions.initialize.exceptions import *
from py_functions.utils.colors import *

def paired_ends(conf, info_sort, study_type):

    cwd = Path.cwd()
    dest = cwd.joinpath("./Analysis/fastq/raw_data")
    dest_merge = cwd.joinpath("./Analysis/fastq/merge")

    conf_file = conf

    pair = ""
    sample = ""

    conf_file['units'] = {}

    for i in range(len(info_sort)):
        src = Path(info_sort.iloc[i,0]).resolve()
        name = dest.joinpath("./{}/{}_{}.fastq.gz".format(info_sort.iloc[i].lane, info_sort.iloc[i].sample_name, info_sort.iloc[i].pair))
        name.symlink_to(src)

        if len(set(info_sort.lane)) == 1:
            name = dest_merge.joinpath("./{}_{}.fastq.gz".format(info_sort.iloc[i].sample_name, info_sort.iloc[i].pair))
            name.symlink_to(src)

        if sample != info_sort.iloc[i].sample_name:
            sample = info_sort.iloc[i].sample_name
            pair = info_sort.iloc[i].pair
            conf_file['units']["{}_{}".format(sample, pair)] = []

        elif sample == info_sort.iloc[i].sample_name and pair != info_sort.iloc[i].pair:
            pair = info_sort.iloc[i].pair
            conf_file['units']["{}_{}".format(sample, pair)] = []

        if sample == info_sort.iloc[i].sample_name and pair == info_sort.iloc[i].pair:
            conf_file['units']["{}_{}".format(sample, pair)].append("Analysis/fastq/raw_data/{}/{}_{}.fastq.gz".format(info_sort.iloc[i].lane, sample, pair))

    return conf_file

def single_ends(info_sort, study_type):

    cwd = Path.cwd()
    dest = cwd.joinpath("./Analysis/fastq/raw_data")
    dest_merge = cwd.joinpath("./Analysis/fastq/merge")
    conf_file = conf
    conf_file['units'] = {}

    sample = ""

    for i in range(len(info_sort)):
        src = Path(info_sort.iloc[i,0]).resolve()
        name = dest.joinpath("./{}/{}.fastq.gz".format(info_sort.iloc[i].lane, info_sort.iloc[i].sample_name))
        name.symlink_to(src)

        if len(list_lanes) == 1:
            name = dest_merge.joinpath("./{}.fastq.gz".format(info_sort.iloc[i].sample_name))
            name.symlink_to(src)

        if sample != info_sort.iloc[i].sample_name:
            sample = info_sort.iloc[i].sample_name
            conf_file['units']["{}".format(sample)] = []

        if sample == info_sort.iloc[i].sample_name:
            conf_file['units']["{}".format(sample)].append("Analysis/fastq/raw_data/{}/{}.fastq.gz".format(info_sort.iloc[i].lane, sample))

    return conf_file



def add_fasta_names(conf, info_sort):

    cwd = Path.cwd()

    samp_list = list(set(info_sort[~info_sort.sample_name.isna()].sample_name))

    conf_file = conf

    conf_file['prefix'] = []
    nb_samp = len(samp_list)

    for sample in samp_list:
        conf_file['prefix'].append(sample)

    return conf_file 
