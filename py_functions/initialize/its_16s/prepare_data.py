
from pathlib import Path
from py_functions.initialize.its_16s import gen_config_files as gcf
from py_functions.initialize.its_16s import gen_config_samples as gcs
from py_functions.initialize.its_16s import gen_study_specific_config as gssc
from py_functions.initialize.its_16s.hck import gen_config_hck as gch


def prepare(info_sheet, conf_file, ini_args, diff_lanes, time_stamp):
    cwd = Path.cwd()
    dict_spec_args = {}
    extras = []

    conf_file['envs'] = {}
    extras = ['HCK']
    hck_conf = Path.cwd().joinpath("config/defaults/hck.yaml")
    conf_file = gch.gen_config_hck(conf_file, hck_conf, ini_args, extras)

    if (len(set(info_sheet.pair)) > 1 and len(set(info_sheet.lane)) >= 0) or ini_args.paired:
        conf_file = gcs.paired_ends(conf_file, info_sheet, ini_args.study_type)
        extras.append('paired')

    ##  The following covers the scenario where we have    SINGLE-ENDS
    elif (len(set(info_sheet.pair)) <= 1 and len(set(info_sheet.lane)) >= 0) or ini_args.single:
        conf_file = gcs.single_ends(info_sheet, ini_args.study_type)
        extras.append('single')
        
    ##  The following adds the fasta file names to the config_samples file
    conf_file = gcs.add_fasta_names(conf_file, info_sheet)

    conf_file = gcf.config_yaml(conf_file, [ini_args.study_type], diff_lanes, time_stamp, extras)
    
    return conf_file  
    
