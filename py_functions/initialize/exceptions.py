
class MissingDirArg(Exception):
    pass

class WrongStudyType(Exception):
    pass

class BadArgument(Exception):
    pass

class MissingFilesDir(Exception):
    pass

class MissingFilesInfoSheet(Exception):
    pass

