####################################################################################################

##  usage : snakemake [OPTIONS] <TARGET_RULES>
##
##  Main snakefile to use in the pipeline.
##  When calling snakemkake, one should specify one or multiple TARGET names to use.
##  The existing TARGET names are found below, next to the RULE keyword.
##
##  Expected OUTFILES.
##  Specify which rule to use prior to starting
##  Snakemake, i.e. :
##
##  snakemake [options] <RULES,...>
##  -- RULES keyword is to be replaced with one
##     or more of the keywords found below, for
##     instance, "pre_livrable"
##
##  See Snakemake --help for more information on all available options

####################################################################################################

## Main configuration file
configfile: "config/config.yaml"


## Set of rules use by the pipeline

## Sets of rules for Step1
include: "rules/step1_preprocessing/abaa.rules"
include: "rules/step1_preprocessing/add_qiime_labels_python.rules"
include: "rules/step1_preprocessing/merge_lanes.rules"
include: "rules/step1_preprocessing/pandaseq.rules"
include: "rules/step1_preprocessing/qc.rules"
include: "rules/step1_preprocessing/trimmomatic.rules"

## Sets of rules for Step2
include: "rules/step2_clustering/clustering.rules"
include: "rules/step2_clustering/merge_clusters.rules"

## Sets of rules for Step3
include: "rules/step3_rep_seq_assig/remove_singletons.rules"
include: "rules/step3_rep_seq_assig/assign_repr_seqs.rules"
include: "rules/step3_rep_seq_assig/extract_seq_from_clusters.rules"

## Sets of rules for Step4
include: "rules/step4_overall_assig/assign.rules"
include: "rules/step4_overall_assig/list_cluster_members.rules"
include: "rules/step4_overall_assig/list_samples.rules"

## Sets of rules for Step5
include: "rules/step5_abundance/abundance.rules"

rule step1: # preprocessing:
    input:
        "Analysis/fasta/combined_seqs.fna",
        expand("Analysis/QC/multiqc_{r_t}.html", r_t = config["multiqc_dir"])

rule step2: #clustering:
    input:
        expand("Analysis/HCK/nr/{c}", c = ["nr99", "nr98", "nr97"]),
        expand("Analysis/HCK/nr/{c}.clstr", c = ["nr99", "nr98", "nr97"]),
        "Analysis/HCK/nr/final.clstr"

rule step3: #rep_seq_assig:
    input:
        "Analysis/HCK/nr/sorted_clusters.clstr",
        "Analysis/HCK/rep/representative_clusters.clstr",
        "Analysis/HCK/rep/rep_metagenome.kraken"

rule step4: #_overall_assig:
    input:
        expand("Analysis/HCK/SAMPLE/{p}.kraken", p = config["prefix"]),

rule step5: #abundance:
    input:
        expand("Analysis/HCK/report/{p}.report", p = config["prefix"]),
        expand("Analysis/HCK/abundance/{lvl}_{aor}.tsv", lvl = ["C", "F", "G", "D", "O", "P", "S"], aor = ["ar", "abs"])
