
from py_functions.initialize import check_deps as cd
cd.check_deps()

import sys
import shutil
import os
import time as t
from pathlib import Path
from py_functions.initialize.exceptions import *
from py_functions.initialize import parse_args as pa
from py_functions.initialize import parse_info_tbl as pit
from py_functions.initialize import parse_yaml as pyml
from py_functions.initialize import sort_info_sheet as sis
from py_functions.utils.colors import *


today = t.strftime('%d%b%y').upper()


cwd = Path.cwd()

##  Get the argument and handle them
args = pa.get_args()

##  Parse the info table and
info_tbl = pit.parse_info_tbl(args.info_sheet)

## Makes a list of all the different lanes
diff_lanes = set(info_tbl[~info_tbl.lane.isna()].lane)

## Checks if fastq directory is already present
## Exits if so, unless --force is used
if 'fastq' in [f.name for f in cwd.joinpath('./Analysis').iterdir()]:
    if args.force:
        list_dir = os.listdir('./Analysis/')
        for directory in list_dir:
            if directory != ".empty":
                shutil.rmtree('./Analysis/{}'.format(directory))

    else:
        print("Directory {0}'fastq'{1} already exists \n"
            "Make sure this directory is empty before proceeding \n"
            "If there is anythng important there, please move it elsewhere and\n"
            "and delete the {0}'fastq'{1} directory.\n"
            "Use {0}--force{1} to force overwrite.".format(YELLOW, DEFAULT))
        exit()


## Makes the directory if it doesn't exist
Path.mkdir(cwd.joinpath("./Analysis/"), exist_ok = True)
Path.mkdir(cwd.joinpath("./Analysis/fastq/"), exist_ok = True)
Path.mkdir(cwd.joinpath("./Analysis/fastq/raw_data/"), exist_ok = True)
for directory in diff_lanes:
    Path.mkdir(cwd.joinpath('./Analysis/fastq/raw_data/{}'.format(directory)), exist_ok = True)


## Creates a symlink to fastq/merge if only one lane
## Allows to skip an extra step since we don't actually need
## to merge multiple lanes.
if len(diff_lanes) == 1:
    Path.mkdir(cwd.joinpath('./Analysis/fastq/merge/'), exist_ok = True)




#######################
##  SORT INFO SHEET  ##
#######################

info_sort = sis.sort_info_sheet(info_tbl)

conf = pyml.load_conf("config/defaults/hck.yaml")


if args.study_type in ['16S', 'ITS']:
## Prep 16S/ITS analysis
    from py_functions.initialize.its_16s import prepare_data as pd
    
    conf = pd.prepare(info_sort, conf, args, diff_lanes, today)


pyml.write_conf(conf, "config/config.yaml", mode = 'w')

