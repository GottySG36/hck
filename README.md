# NOT SUPPORTED ANYMORE

The main creator is no longer supporting this project. It remains available as is, 
but no requests will be answered regarding any issues. 

You are free to use anything found on this repository, as long as proper credits are provided.

# HCK -:- New and innovative technique envisioned to allow for better and more accurate taxonomic assignment of metagenomic datasets.

## Base procedure to get started

First, install the latest version of conda. The latest installer can be found [here](https://docs.conda.io/en/latest/miniconda.html). We suggest using miniconda as it is a lot lighter and in any case we don't need all the included packages found in the full installer. Every thing we need will be isntalled by the pipeline except for one package (see [here](#Initial-setup)).

You will also need...? This project!! So go ahead and clone this repository : 

`git clone https://GottySG36@bitbucket.org/GottySG36/hck.git`

### Initial setup

With conda installed, all we need before we get started is to install the pipeline manager, which will take care of the workflow for us. The manager we will be using is [Snakemake](https://snakemake.readthedocs.io/en/stable/). It can easily be installed within conda with the following command.

`conda install -c bioconda -c conda-forge snakemake`

Before we start our analysis, we need to get the pipeline manager, which will take care of the workflow for us. The manager we will be using is [Snakemake](https://snakemake.readthedocs.io/en/stable/).

In order to install it, we need to create a virtual environment which will contain it and it's other dependencies. You can do this with which ever tool you want, but here, we will be using python's venv.

`python3 -m venv <env-name>`

Once done activate the environment :

`source <env-name>/bin/activate`

Only then, we install the requirements with the following command.

`python3 -m pip install -r requirements.txt`



### Starting the analysis

Assuming you will be using all the default parameters, all you need to do to start the analysis is to type `snakemake` within the main directory of the repository (`hck/`).


### General structure

This pipeline is divided in five (5) steps.

1. Pre-processing, quality filtering and chimera removal;
2. clustering;
3. Representative sequences taxonomic assignment;
4. Complete taxonomic assignment;
5. Abundance calculation.


#### samples-info-sheet.csv

The goal of this file is to tell the preparation script where the
samples are located and to dictate how the symbolic links will be named.
Depending on whether the data is paired or not, there are going to be
either 3 or 4 columns.

This needs to be done manually, but a simple script can be written in a
few seconds to accomplish this more quickly.

##### Paired-ends

Since paired-ends require an extra bit of information, the info-sheet
will have 4 columns in this scenario. It should look as follows:

```
raw_files,sample_name,pair,lane
raw_sample1_R1_L1,renamed_sample1,R1,lane1
raw_sample1_R2_L1,renamed_sample1,R2,lane1
raw_sample1_R1_L2,renamed_sample1,R2,lane2
raw_sample1_R2_L2,renamed_sample1,R2,lane2
raw_sample2_R1_L1,renamed_sample2,R1,lane1
[...]
```

##### Single-ends

It the case of single-ends, we may omit the *pair* column. The rest is the
unchanged.

```
raw_files,sample_name,lane
raw_sample1_L1,renamed_sample1,lane1
raw_sample1_L2,renamed_sample1,lane2
raw_sample2_L1,renamed_sample2,lane1
[...]
```

It is very important to respect the column names. Although the order is
not important, if misspelled, the script will raise an error. Also, **DO
NOT** put the *"pair"* column if not required. That column is used to
detect wether we are working with paired-ends or not. Currently, this
utility doesn't support dual sequence types.

***IMPORTANT***

As mentioned in the previous section, if the raw sample file names do
not match between the different lanes, the `sample_name` field in the
samples-info-sheet needs to be used to fix that. Regardless of the
original names, this field dictates how the files are going to be named.
At the very beginning of the analysis, the different lanes are merged.
This is done based on the file names. If the names aren't identical,
Snakemake can't determine which file goes with which.

------------------------------------------------------------------------

### Preparing Snakemake

Before running Snakemake, we need to prepare the work directory
(`Snakemake_workflows/.`). In the `hck/` directory, you
will find a script named `prepare_snakemake.py`. This script takes an
info sheet (described above) and takes cares of creating the required
directories and symbolic links. 

`python3 prepare_snakemake.py --info-sheet=<PATH/TO/INFO-SHEET>`

------------------------------------------------------------------------

### Launching the analysis

If no error occurred, it is time to start analyzing our data. At this
point, it is just a matter of running Snakemake. The only thing
important to know is that the different study types is subdivided in one
or more parts.

Look in `hck/DOCS/` for more information concerning each 
step and to get a global view of the workflow.

Before running the analysis, it is recommended to do a *dry-run* to make
sure every thing is ready. Depending on what you want to do, you may want  
to tell Snakemake with part you want to run. If it
requires that another part be run first, Snakemake should normally
figure it out and run it before. But it can't hurt to specify all of
them anyway.

`snakemake -np step1 step2 step3 step4 step5`

Here, we use to options :

`-n` : **Dry-Run**. This means that we do not actually run the pipeline,
just print what will be done. This is useful to make sure that all the
files are where they should be and that nothing is missng or are
improperly configured. (Doesn't guarantee that there won't be any errors
though).

`-p` : **Print commands**. Gives a little more information on what is
going to be done.

Here, the snakemake options we commonly use will be presented, but many
more exist. See Snakemake's documentation for more information.

Now, to start the analysis, one only needs to type `snakemake`.
Although, Snakemake will only run the first rule found in the Snakefile.
This will work, but you will miss some quality checks hich are sometimes 
informative. To complete the entire analysis,
you should add the parts you wish to run. For instance :

`snakemake step1 step5`

Also, since we use conda environments, we need to tell Snakemake
using other options. Here the relevant ones are `--use-conda` and
`--conda-prefix <path/to/env/install/dir>`.

The first simply tells that we need to use conda. The second is not
necessary to work. But since we want our environments in a specific
location, we need to use it.

`snakemake --use-conda --conda-prefix .conda-env/ step5`

You can always specify how many threads are to be used at most at the
same time. This is done with the `-j` option.

`snakemake --use-conda --conda-prefix .conda-env/ -j12 step5`

If you wish to have more information on what commands are used, you can
add, once again, the `-p` option.

`snakemake --use-conda --conda-prefix .conda-env/ -pj12 step5`

